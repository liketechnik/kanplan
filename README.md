<!--
SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>

SPDX-License-Identifier: MPL-2.0

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
-->

# Kanplan (WIP)

Server-Client based Kanban board implementation.

## Development Quickstart

### Server

- start postgresql db server: `docker-compose up -d`
- create database and tables: `cargo sqlx setup` (install with `cargo install sqlx-cli`)

# License

This Source Code is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
