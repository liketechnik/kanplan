<!--
SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>

SPDX-License-Identifier: MPL-2.0

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
-->

# Core features

- swimlanes
- boards
- cards
	- appoint time (from / till, optionally full day)
	- deadline until work must be finished
- labels
	- global
	- local
	- automatically (derived?), example use for appointments, deadlines, etc
	- different kinds?
	- labels can fire actions
- links between elements
- columns
- automatic assignments based on label
- projects

# Extended features

- timetracking
- integration with github, gitlab, other kinds of forums, rss etc (e. g. automatic synchronisation with issues)
- interface via webdav or similiar
- versioning of content
- notifications via email, rss, etc
- text formatting with markdown
- usage for organizing group work

# Non-functional features
- common server-API to support multiple clients
- Web-, Desktop-, (Mobile-) Clients, Server
- multiple DB backends (postgresql, sqlite, mysql/mariadb)

# Frameworks

- [Iced](https://github.com/hecrj/iced) - gui framework
- [Tide](https://docs.rs/tide/0.16.0/tide/) - http framework
- [SQLx](https://github.com/launchbadge/sqlx) - db integration
