# SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
#
# SPDX-License-Identifier: CC0-1.0

{
  description = "Kanplan development flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-20.09";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    mozilla = {
      url = "github:mozilla/nixpkgs-mozilla"; # provides rust nightlies
      flake = false; # see https://github.com/mozilla/nixpkgs-mozilla/pull/248
    };
  };

  outputs = { self, nixpkgs, flake-utils, nixpkgs-unstable, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # create a new overlay
        overlay-unstable = self: super: {
          # expose the packages from nixpkgs-unstable under the unstable attribute
          unstable = inputs.nixpkgs-unstable.legacyPackages.${super.system};
        };
        pkgs = import nixpkgs {
          overlays = [ overlay-unstable ];

          inherit system;
        };
      in rec {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs.unstable; [ 
            # rust tools
            cargo
            rustc
            rustfmt
            rust-analyzer
            clippy

            # deps
            pkgs.openssl
            pkgs.pkg-config
            pkgs.cmake
            pkgs.gtk3

            # additional tools
            sqlx-cli
          ];

          # See https://discourse.nixos.org/t/rust-src-not-found-and-other-misadventures-of-developing-rust-on-nixos/11570/3?u=samuela.
          RUST_SRC_PATH = "${pkgs.unstable.rust.packages.stable.rustPlatform.rustLibSrc}";
        };
      }
    );
}
