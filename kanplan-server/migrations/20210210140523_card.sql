-- SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
--
-- SPDX-License-Identifier: MPL-2.0
--
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

CREATE TABLE IF NOT EXISTS card
(
	id    BIGSERIAL  PRIMARY KEY,
	title    TEXT  NOT NULL,
	description    TEXT  NOT NULL
);
