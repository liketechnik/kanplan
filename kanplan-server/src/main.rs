// SPDX-FileCopyrightText: 2021 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::{sync::Arc, time::Duration};

use kanplan_core::{Card, Identifiable};

use sqlx::postgres::{PgConnectOptions, PgPoolOptions, Postgres};
use sqlx::{pool::PoolConnection, Connection, Pool};
use tide::{
    convert::json, http::mime, utils::After, Body, Middleware, Next, Request, Response, Result,
    StatusCode,
};
use tracing::{error, info};
use tracing_subscriber::EnvFilter;

struct DatabaseMiddleware {
    db: Arc<Pool<Postgres>>,
}

impl DatabaseMiddleware {
    async fn new() -> anyhow::Result<Self> {
        let connect_opts = PgConnectOptions::new()
            .host("localhost")
            .port(5433)
            .database("kanplan")
            .username("postgres")
            .password("password");
        let pg_pool = PgPoolOptions::new()
            .max_connections(5)
            .min_connections(2)
            .connect_timeout(Duration::new(5, 0))
            .connect_with(connect_opts)
            .await?;
        pg_pool.acquire().await?.ping().await?;
        info!("Connected to database");
        Ok(Self {
            db: Arc::new(pg_pool),
        })
    }
}

#[tide::utils::async_trait]
impl<State: Clone + Send + Sync + 'static> Middleware<State> for DatabaseMiddleware {
    async fn handle(&self, mut req: Request<State>, next: Next<'_, State>) -> Result {
        req.set_ext(self.db.acquire().await?);

        Ok(next.run(req).await)
    }
}

async fn handle_deserialization_err<T>(result: tide::Result<T>) -> tide::Result<T> {
    match result {
        Ok(value) => Ok(value),
        Err(mut response) => {
            response.set_status(StatusCode::BadRequest);
            Err(response)
        }
    }
}

async fn get_cards(mut req: tide::Request<()>) -> tide::Result {
    let db: &mut PoolConnection<Postgres> = req.ext_mut().unwrap();

    let cards = sqlx::query_as!(
        Identifiable::<Card>,
        r#"SELECT id, (title, description) as "value!: Card" FROM card"#
    )
    .fetch_all(db)
    .await?;

    let mut req = Response::new(StatusCode::Ok);
    let body = Body::from_json(&cards)?;
    req.set_body(body);

    Ok(req)
}

async fn add_card(mut req: tide::Request<()>) -> tide::Result {
    let card: Card = handle_deserialization_err(req.body_json::<Card>().await).await?;

    let db: &mut PoolConnection<Postgres> = req.ext_mut().unwrap();
    sqlx::query!(
        r#"INSERT INTO card (title, description) VALUES ($1, $2)"#,
        card.title,
        card.description
    )
    .fetch_optional(db)
    .await?;

    let mut req = Response::new(StatusCode::Ok);
    let body = Body::from_json(&json!({"detail": "success"}))?;
    req.set_body(body);
    req.set_content_type(mime::JSON);

    Ok(req)
}

#[async_std::main]
async fn main() -> anyhow::Result<()> {
    let filter = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new("info"))
        .unwrap();
    tracing_subscriber::fmt().with_env_filter(filter).init();

    let mut app = tide::new();

    app.with(DatabaseMiddleware::new().await?);
    app.with(After(|mut response: Response| async move {
        let response = match response.status() {
            StatusCode::InternalServerError => {
                if let Some(e) = response.error() {
                    error!("{:?}", e);
                    response = Response::new(StatusCode::InternalServerError);
                    response.set_content_type(mime::JSON);
                    response.set_body(Body::from_json(
                        &json!({"detail": "internal server error"}),
                    )?);
                }
                response
            }
            StatusCode::BadRequest => match response.error() {
                None => response,
                Some(e) => {
                    let mut req = Response::new(StatusCode::BadRequest);
                    req.set_content_type(mime::JSON);
                    req.set_body(Body::from_json(&json!({ "detail": format!("{:?}", e) }))?);
                    req
                }
            },
            _ => response,
        };

        Ok(response)
    }));

    app.at("/").get(|mut req: tide::Request<()>| async move {
        let db: &mut PoolConnection<Postgres> = req.ext_mut().unwrap();

        let row: (i64,) = sqlx::query_as("SELECT $1")
            .bind(150_i64)
            .fetch_one(db)
            .await?;

        Ok(format!(
            "Hello Tide + Sqlx :)\nFetched {} from the db.",
            row.0
        ))
    });
    app.at("/api/v1/card").get(get_cards).post(add_card);

    app.listen("127.0.0.1:8080").await?;
    Ok(())
}
